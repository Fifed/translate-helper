package com.fifed.architecture_datacontroller.interaction.core;


/**
 * Created by Fedir on 05.07.2016.
 */
public class Error {
    private String TAG;
    private String globalErrorMessage;


    public Error(String TAG, String globalErrorMessage) {
        this.TAG = TAG;
        this.globalErrorMessage = globalErrorMessage;
    }


    public String getTAG() {
        return TAG;
    }

    public String getGlobalErrorMessage() {
        return globalErrorMessage;
    }



}
