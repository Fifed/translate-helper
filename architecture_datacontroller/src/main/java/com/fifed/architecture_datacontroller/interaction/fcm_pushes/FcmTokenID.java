package com.fifed.architecture_datacontroller.interaction.fcm_pushes;

import com.fifed.architecture_datacontroller.interaction.fcm_pushes.core.FcmPush;

/**
 * Created by Fedir on 10.07.2016.
 */
public class FcmTokenID implements FcmPush {
    String tokenID;

    public String getTokenID() {
        return tokenID;
    }

    public FcmTokenID setTokenID(String tokenID) {
        this.tokenID = tokenID;
        return this;
    }
}
