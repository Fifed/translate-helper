package com.fifed.architecture_datacontroller.interactor.observer.interfaces;


import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;

/**
 * Created by Fedir on 05.07.2016.
 */
public interface ObservableInteractor {
    void registerObserver(ObserverInteractor observer);
    void unregisterObserver(ObserverInteractor observer);
    void notifyObserversOnUpdateData(Model model);
    void notifyObserversOnError(Error error);
}
