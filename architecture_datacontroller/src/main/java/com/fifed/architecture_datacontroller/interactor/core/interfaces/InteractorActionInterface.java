package com.fifed.architecture_datacontroller.interactor.core.interfaces;


import com.fifed.architecture_datacontroller.interaction.core.Action;

/**
 * Created by Fedir on 05.07.2016.
 */
public interface InteractorActionInterface {
    void onUserAction(Action action);
}
