package com.fifed.inputlayoutviews.utils.validators;


import android.content.Context;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

/**
 * Created by Fedir on 13.07.2016.
 */
public class ValidatorEmail implements TextValidator {
    @Override
    public ValidatorResponse isValidText(String text, Context context) {
        if (text.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            return new ValidatorResponse(null, true);
        } else return new ValidatorResponse(context.getString(R.string.error_valid_email), false);
    }
}
