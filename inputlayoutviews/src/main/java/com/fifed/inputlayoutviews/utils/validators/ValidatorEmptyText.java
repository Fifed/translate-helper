package com.fifed.inputlayoutviews.utils.validators;


import android.content.Context;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

/**
 * Created by Fedir on 13.07.2016.
 */
public class ValidatorEmptyText implements TextValidator {
    @Override
    public  ValidatorResponse isValidText(String text, Context context) {
        if(text.length() == 0){
            return new ValidatorResponse(context.getString(R.string.error_valid_empty_text), false);
        } else return new ValidatorResponse(null, true);
    }
}

