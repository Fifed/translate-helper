package com.fifed.inputlayoutviews.utils.validators;


import android.content.Context;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

/**
 * Created by Fedir on 05.08.2016.
 */
public class ValidatorPhone implements TextValidator {
    @Override
    public ValidatorResponse isValidText(String text, Context context) {
        if(text.length() == 0 || text.equals("+380")){
            return new ValidatorResponse(null, true);
        }
        if(text.matches("^[+]{1}[0-9]{8,14}+$")){
            return new ValidatorResponse(null, true);
        } else return new ValidatorResponse(context.getString(R.string.not_correct_num_phone), false);
    }
}
