package com.fifed.inputlayoutviews.utils.validators;

import android.content.Context;
import android.widget.EditText;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;


/**
 * Created by Fedir on 04.08.2016.
 */
public class ValidatorsEqualsPasswords implements TextValidator {
    private EditText editTextPasword, editTextRepeatPassword;

    public ValidatorsEqualsPasswords(EditText editTextPasword, EditText editTextRepeatPassword) {
        this.editTextPasword = editTextPasword;
        this.editTextRepeatPassword = editTextRepeatPassword;
    }

    @Override
    public ValidatorResponse isValidText(String text, Context context) {
        if(editTextPasword.getText().toString().equals(editTextRepeatPassword.getText().toString())){
            return new ValidatorResponse(null, true);
        } else return new ValidatorResponse(context.getString(R.string.passwords_must_be_same), false);
    }
}
