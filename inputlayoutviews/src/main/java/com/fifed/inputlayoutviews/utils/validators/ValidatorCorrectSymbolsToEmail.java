package com.fifed.inputlayoutviews.utils.validators;


import android.content.Context;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

/**
 * Created by Fedir on 15.07.2016.
 */
public class ValidatorCorrectSymbolsToEmail implements TextValidator {
    @Override
    public ValidatorResponse isValidText(String text, Context context) {
        if(text.matches("^[A-Za-z0-9.@_]+$")){
            return new ValidatorResponse(null, true);
        } else return new ValidatorResponse(context.getString(R.string.errror_valid_email_symbols), false);
    }
}
