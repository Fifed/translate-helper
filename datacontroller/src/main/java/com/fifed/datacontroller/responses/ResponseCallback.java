package com.fifed.datacontroller.responses;

import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;

/**
 * Created by Fedir on 01.12.2016.
 */

public interface ResponseCallback {
    void onSuccess(Model model);
    void onError(Error error);
}
