package com.fifed.datacontroller.dictionary;

import android.content.Context;

import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.datacontroller.R;
import com.fifed.datacontroller.dictionary.interfaces.DictionaryManager;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromDictionaryAction;
import com.fifed.datacontroller.interactions.models.TranslateFromDictionaryModel;
import com.fifed.datacontroller.responses.ResponseCallback;
import com.fifed.dictionary.Dictionary;
import com.fifed.dictionary.models.TranslatedWords;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Fedir on 01.12.2016.
 */

public class DevDictionaryManager implements DictionaryManager {
    Context context;
    Subscription subscription;

    public DevDictionaryManager(Context context) {
        this.context = context;
    }

    @Override
    public void getTranslatedWords(GetTranslateFromDictionaryAction action, ResponseCallback response) {
       subscription = Observable.just(action)
                .subscribeOn(Schedulers.newThread())
                .flatMap(action1 -> Observable.just(Dictionary.getInstance().searchWords(action.getWordToTranslate())))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(strings -> {
                   response.onSuccess(new TranslateFromDictionaryModel(action, new TranslatedWords(strings)));
                   subscription.unsubscribe();
                   }, error -> {
                     response.onError(new Error(action.getTAG(), context.getString(R.string.error_getting_data)));
                     subscription.unsubscribe();
                });
    }
}
