package com.fifed.datacontroller.dictionary.interfaces;

import com.fifed.datacontroller.interactions.actions.GetTranslateFromDictionaryAction;
import com.fifed.datacontroller.responses.ResponseCallback;

/**
 * Created by Fedir on 01.12.2016.
 */

public interface DictionaryManager {
   void getTranslatedWords(GetTranslateFromDictionaryAction action, ResponseCallback response);
}
