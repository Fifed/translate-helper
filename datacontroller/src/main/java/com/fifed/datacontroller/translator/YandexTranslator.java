package com.fifed.datacontroller.translator;

import android.content.Context;

import com.fifed.architecture_datacontroller.interaction.core.Action;
import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.datacontroller.R;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromInternetAction;
import com.fifed.datacontroller.interactions.models.TranslateFromInternetModel;
import com.fifed.datacontroller.responses.ResponseCallback;
import com.fifed.datacontroller.translator.interfaces.Translator;
import com.fifed.translate_sdk.TranslateApi;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Fedir on 02.12.2016.
 */

public class YandexTranslator implements Translator {
    Context context;
    TranslateApi api;
    Subscription subscription;
    public YandexTranslator(Context context) {
        this.context = context;
        TranslateApi.init(context);
        api = TranslateApi.getInstance();
    }

    private Error handleErrors(Action action, Throwable t){
        if(t instanceof HttpException && ((HttpException)t).code() == 504){
                return new Error(action.getTAG(), "No Internet connection. Try to continue to write yor text, it can be cached");
        } else if(t instanceof UnknownHostException){
            return new Error(action.getTAG(), context.getString(R.string.no_internet_connection));
        } else if(t instanceof SocketTimeoutException) {
            return new Error(action.getTAG(), context.getString(R.string.server_is_not_responding));
        } else return new Error(action.getTAG(), null);
    }

    @Override
    public void translate(GetTranslateFromInternetAction action, ResponseCallback response) {
        subscription = Observable.just(action)
                .subscribeOn(Schedulers.newThread())
                .flatMap(a -> api.translate(action.getTextToTranslate()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> {
                    response.onSuccess(new TranslateFromInternetModel(action, res));
                    subscription.unsubscribe();
                }, t -> {
                    response.onError(YandexTranslator.this.handleErrors(action, t));
                    subscription.unsubscribe();
                });
    }
}
