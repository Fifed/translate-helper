package com.fifed.datacontroller.translator.interfaces;

import com.fifed.datacontroller.interactions.actions.GetTranslateFromInternetAction;
import com.fifed.datacontroller.responses.ResponseCallback;

/**
 * Created by Fedir on 02.12.2016.
 */

public interface Translator {
    public void translate(GetTranslateFromInternetAction action, ResponseCallback response);
}
