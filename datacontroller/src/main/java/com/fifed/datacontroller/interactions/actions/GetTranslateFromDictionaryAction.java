package com.fifed.datacontroller.interactions.actions;

import com.fifed.architecture_datacontroller.interaction.core.Action;

/**
 * Created by Fedir on 01.12.2016.
 */

public class GetTranslateFromDictionaryAction extends Action {
    private String wordToTranslate;
    public GetTranslateFromDictionaryAction(String TAG, String wordToTranslate) {
        super(TAG);
        this.wordToTranslate = wordToTranslate;
    }

    public String getWordToTranslate() {
        return wordToTranslate;
    }
}
