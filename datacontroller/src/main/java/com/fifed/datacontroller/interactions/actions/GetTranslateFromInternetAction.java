package com.fifed.datacontroller.interactions.actions;

import com.fifed.architecture_datacontroller.interaction.core.Action;

/**
 * Created by Fedir on 02.12.2016.
 */

public class GetTranslateFromInternetAction extends Action{
    private String textToTranslate;
    public GetTranslateFromInternetAction(String TAG, String textToTranslate) {
        super(TAG);
        this.textToTranslate = textToTranslate;
    }

    public String getTextToTranslate() {
        return textToTranslate;
    }
}
