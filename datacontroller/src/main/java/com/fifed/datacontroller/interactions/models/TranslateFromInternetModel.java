package com.fifed.datacontroller.interactions.models;

import com.fifed.architecture_datacontroller.interaction.core.Action;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.datacontroller.interactions.models.models_id.TranslateFromInternetModelID;
import com.fifed.translate_sdk.beans.intarfaces.ITranslated;

/**
 * Created by Fedir on 02.12.2016.
 */

public class TranslateFromInternetModel extends Model implements TranslateFromInternetModelID {
    private ITranslated translated;
    public TranslateFromInternetModel(Action action, ITranslated translated) {
        super(action);
        this.translated = translated;
    }

    public ITranslated getTranslate() {
        return translated;
    }
}
