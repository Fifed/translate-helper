package com.fifed.datacontroller.interactions.models;

import com.fifed.architecture_datacontroller.interaction.core.Action;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.datacontroller.interactions.models.models_id.TranslateFromDictionaryModelID;
import com.fifed.dictionary.models.intarfaces.ITranslatedWords;

/**
 * Created by Fedir on 01.12.2016.
 */

public class TranslateFromDictionaryModel extends Model implements TranslateFromDictionaryModelID {
    ITranslatedWords translatedWords;
    public TranslateFromDictionaryModel(Action action, ITranslatedWords translatedWords) {
        super(action);
        this.translatedWords = translatedWords;
    }

    public ITranslatedWords getTranslatedWords() {
        return translatedWords;
    }
}
