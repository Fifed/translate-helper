package com.fifed.datacontroller.interactors;

import android.content.Context;

import com.fifed.architecture_datacontroller.interaction.core.Action;
import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.architecture_datacontroller.interactor.core.BaseInteractor;
import com.fifed.datacontroller.dictionary.DevDictionaryManager;
import com.fifed.datacontroller.dictionary.interfaces.DictionaryManager;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromDictionaryAction;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromInternetAction;
import com.fifed.datacontroller.responses.ResponseCallback;
import com.fifed.datacontroller.translator.YandexTranslator;
import com.fifed.datacontroller.translator.interfaces.Translator;
import com.fifed.dictionary.Dictionary;

/**
 * Created by Fedir on 01.12.2016.
 */

public class TranslateInteractor extends BaseInteractor {
    private static TranslateInteractor interactor;
    private DictionaryManager dictionaryManager;
    private Translator translator;
    private TranslateInteractor(Context context) {
        super(context);
        dictionaryManager = new DevDictionaryManager(context);
        translator = new YandexTranslator(context);
    }
    public static TranslateInteractor getInteractorInstance(Context context){
        if(interactor == null){
             interactor = new TranslateInteractor(context);
                    Dictionary.init(context);
        }
        return interactor;
    }

    @Override
    public void onUserAction(Action action) {
        if(action instanceof GetTranslateFromDictionaryAction){
             handleGetTranslateFromDictionaryAction((GetTranslateFromDictionaryAction) action);
        } else if(action instanceof GetTranslateFromInternetAction){
            handleGetTranslateFromInternetAction((GetTranslateFromInternetAction) action);
        }
    }

    private void handleGetTranslateFromDictionaryAction(GetTranslateFromDictionaryAction action){
        dictionaryManager.getTranslatedWords(action, new ResponseCallback() {
            @Override
            public void onSuccess(Model model) {
                notifyObserversOnUpdateData(model);
            }

            @Override
            public void onError(Error error) {
                notifyObserversOnError(error);
            }
        });
    }

    private void handleGetTranslateFromInternetAction(GetTranslateFromInternetAction action){
        translator.translate(action, new ResponseCallback() {
            @Override
            public void onSuccess(Model model) {
                notifyObserversOnUpdateData(model);
            }

            @Override
            public void onError(Error error) {
                notifyObserversOnError(error);
            }
        });
    }
}
