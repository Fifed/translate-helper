package com.fifed.architecture_app.observers;


import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;

/**
 * Created by Fedir on 30.06.2016.
 */
public interface ObservebleActivity {
    void registerObserver(ObserverActivity obsever);
    void unregisterObserver(ObserverActivity observer);
    boolean notifyOnBackPressed();
    void notifyObserversOnUpdateData(Model model);
    void notifyObserversOnError(Error error);
}
