package com.fifed.architecture_app.mvp.presenters.intefaces;


import com.fifed.architecture_datacontroller.interaction.core.Action;

/**
 * Created by Fedir on 30.06.2016.
 */
public interface Presenter {
    void onUserMadeAction(Action action);

    void onPresenterDestroy();
}
