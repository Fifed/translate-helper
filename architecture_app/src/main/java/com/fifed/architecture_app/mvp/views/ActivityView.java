package com.fifed.architecture_app.mvp.views;


import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;

/**
 * Created by Fedir on 30.06.2016.
 */
public interface ActivityView {
    void onError(Error error);
    void onUpdateData(Model model);
}
