package com.fifed.architecture_app.activities.interfaces;


import com.fifed.architecture_datacontroller.interaction.core.Action;

/**
 * Created by Fedir on 05.07.2016.
 */
public interface ActivityActionInterface {
    void userMadeAction(Action action);
}
