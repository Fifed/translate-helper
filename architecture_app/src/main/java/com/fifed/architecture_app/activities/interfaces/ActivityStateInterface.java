package com.fifed.architecture_app.activities.interfaces;

/**
 * Created by Fedir on 06.12.2016.
 */

public interface ActivityStateInterface {
    boolean isAfterSaveInstanceState();
    boolean isActivityRotated();
}
