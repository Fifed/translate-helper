package com.fifed.translate_sdk.servises;

import com.fifed.translate_sdk.TranslateApi;
import com.fifed.translate_sdk.beans.responses.TranslateResponse;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Fedir on 02.12.2016.
 */

public interface TranslateService {
    @GET(TranslateApi.TRANSLATE_PATH)
    @Headers(TranslateApi.X_HARD_CACHE_HEADER)
    Observable<TranslateResponse> translate(@Query("key")String key, @Query("lang")String lang, @Query("text")String text);
}
