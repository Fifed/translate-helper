package com.fifed.translate_sdk;

import android.content.Context;

import com.fifed.translate_sdk.utils.ClientService;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.fifed.translate_sdk.TranslateApi.BASE_URL;

/**
 * Created by Fedir on 02.12.2016.
 */

public class TranslateRestClient {
    private static TranslateRestClient restClient;
    private Retrofit retrofit;
    private TranslateRestClient(Context context){
        initRetrofit(context);
    }
    private void initRetrofit(Context context){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(ClientService.getClient(context))
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
    public <S> S createService(Class<S> service){
       return retrofit.create(service);
    }
    public static void init(Context context){
        if(restClient == null){
            restClient = new TranslateRestClient(context);
        }
    }


    public static TranslateRestClient getInstance(){
        if(restClient == null){
            throw new RuntimeException("First init TranslateRestClient");
        } else return restClient;
    }
}
