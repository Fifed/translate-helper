package com.fifed.translate_sdk;

import android.content.Context;

import com.fifed.translate_sdk.beans.Lang;
import com.fifed.translate_sdk.beans.responses.TranslateResponse;
import com.fifed.translate_sdk.servises.TranslateService;
import com.fifed.translate_sdk.utils.InternetUtils;

import rx.Observable;

/**
 * Created by Fedir on 02.12.2016.
 */

public class TranslateApi {
    private static TranslateApi api;
    private TranslateRestClient restClient;
    private TranslateService service;
    public static final String TRANSLATE_PATH = "/api/v1.5/tr.json/translate";
    public static final String API_KEY = "trnsl.1.1.20160518T165940Z.a460126b0af61943.76edcaf590aaad23f96d3010f3902a2214fed963";
    public static final String BASE_URL = "https://translate.yandex.net";


    public static final String X_OFFLINE_CACHE_KEY = "X-Offline";
    public static final String X_HARD_CACHE_KEY = "X-Hard";
    public static final String CACHE_LIVE_TIME_VAL = "200000"; // 3 години кеш буде валідним

    /**
     * Якщо є інтернет то брати дані з кеша, при відсутності помилка
     */
    public static final String X_ONLINE_CACHE_HEADER = "X-Cache: " + CACHE_LIVE_TIME_VAL;

    /**
     * Якщо є інтернет то обновити дані, при відсутності брати з кеша
     */
    public static final String X_OFFLINE_CACHE_HEADER = X_OFFLINE_CACHE_KEY + ": true";

    /**
     * Якщо є інтернет то брати дані з кеша, при відсутності брати дані з кеша,
     */
    public static final String X_HARD_CACHE_HEADER = X_HARD_CACHE_KEY + ": true";


    public static void init(Context context){
        if(api == null){
            api = new TranslateApi(context);
        }
    }
    private TranslateApi(Context context) {
        InternetUtils.ini(context);
        TranslateRestClient.init(context);
        restClient = TranslateRestClient.getInstance();
        service = restClient.createService(TranslateService.class);
    }
    public static TranslateApi getInstance(){
        if(api == null){
            throw  new RuntimeException("First Init Api");
        } else return api;
    }
    public Observable<TranslateResponse> translate(String text){
       return service.translate(API_KEY, Lang.ENG.getLang()+"-"+Lang.RUS.getLang(), text);
    }
}
