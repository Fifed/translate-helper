package com.fifed.translate_sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetUtils {
    private Context context;
    private static InternetUtils internetUtils;

    private InternetUtils(Context context) {
        this.context = context;
    }

    public static void ini(Context context) {
        internetUtils = new InternetUtils(context);
    }

    public static InternetUtils getInstiance() {
        return internetUtils;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
