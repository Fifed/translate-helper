package com.fifed.translate_sdk.utils.interceptors;

import android.text.TextUtils;

import com.fifed.translate_sdk.utils.InternetUtils;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.fifed.translate_sdk.TranslateApi.CACHE_LIVE_TIME_VAL;
import static com.fifed.translate_sdk.TranslateApi.X_HARD_CACHE_KEY;
import static com.fifed.translate_sdk.TranslateApi.X_OFFLINE_CACHE_KEY;

public class OfflineCacheControlInterceptor implements Interceptor {
    /*
        max-age - Timeout value in seconds to hit again.
        no-cache - Do not use the cache.
        no-store - Do not store the cache.
        only-if-cached - Check the cache before trying to hit.
    */

    private Cache cache;

    public OfflineCacheControlInterceptor(Cache cache) {
        this.cache = cache;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        boolean reqHasOfflineCacheHeader = hasOfflineMode(request);
        boolean reqHasHardCacheHeader = hasHardMode(request);
        boolean hasInternet = InternetUtils.getInstiance().isNetworkAvailable();
        boolean isGetReq = request.method().equals("GET");

        if (isGetReq) {
            if (hasInternet) {
                if (reqHasOfflineCacheHeader) {
                    request = request
                            .newBuilder()
                            .header("Cache-Control", "no-cache")
                            .build();
                } else if (reqHasHardCacheHeader) {
                    request = request
                            .newBuilder()
                            .header("Cache-Control", "public , max-stale=" + CACHE_LIVE_TIME_VAL)
                            .build();
                }
            } else if (reqHasOfflineCacheHeader || reqHasHardCacheHeader) {
                request = request
                        .newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + CACHE_LIVE_TIME_VAL)
                        .build();
            }
        }

        return chain.proceed(request);
    }

    private boolean hasOfflineMode(Request request) {
        String headerVal = request.headers().get(X_OFFLINE_CACHE_KEY);
        return !TextUtils.isEmpty(headerVal);
    }

    private boolean hasHardMode(Request request) {
        String headerVal = request.headers().get(X_HARD_CACHE_KEY);
        return !TextUtils.isEmpty(headerVal);
    }
}