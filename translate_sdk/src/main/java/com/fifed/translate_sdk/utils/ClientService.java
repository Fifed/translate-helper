package com.fifed.translate_sdk.utils;

import android.content.Context;
import android.util.Log;

import com.fifed.translate_sdk.utils.interceptors.LogsInterceptor;
import com.fifed.translate_sdk.utils.interceptors.OfflineCacheControlInterceptor;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class ClientService {

    public static OkHttpClient getClient(Context context) {
        Cache cache = provideCache(context);

        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(15,TimeUnit.SECONDS)
                .writeTimeout(15,TimeUnit.SECONDS)
                .cache(cache)
                .addInterceptor(new OfflineCacheControlInterceptor(cache))
                .addInterceptor(new LogsInterceptor())
                .build();
    }

    private static Cache provideCache(Context context) {
        Cache cache = null;
        try {
            cache = new Cache(new File(context.getCacheDir(), "http-cache"),
                    50 * 1024 * 1024); // 50 MB
        } catch (Exception e) {
            Log.d("Retrofit", "Could not create Cache!");
        }
        return cache;
    }
}