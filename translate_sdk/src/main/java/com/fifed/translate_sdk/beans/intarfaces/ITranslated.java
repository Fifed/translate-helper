package com.fifed.translate_sdk.beans.intarfaces;

/**
 * Created by Fedir on 02.12.2016.
 */

public interface ITranslated {
    public String getTranslatedText();
}
