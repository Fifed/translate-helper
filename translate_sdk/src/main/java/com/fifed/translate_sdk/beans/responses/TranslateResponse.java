package com.fifed.translate_sdk.beans.responses;

import com.fifed.translate_sdk.beans.intarfaces.ITranslated;

/**
 * Created by Fedir on 02.12.2016.
 */

public class TranslateResponse implements ITranslated {
    private int code;
    private String lang;
    private String[] text;

    @Override
    public String getTranslatedText() {
        if(text!=null && text.length > 0){
            return text[0];
        } else return null;
    }
}
