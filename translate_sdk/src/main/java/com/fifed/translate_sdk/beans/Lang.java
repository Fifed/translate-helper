package com.fifed.translate_sdk.beans;

/**
 * Created by Fedir on 02.12.2016.
 */

public enum Lang {
    RUS("ru"),
    ENG("en");

    private String lang;
    Lang(String lang) {
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }
}
