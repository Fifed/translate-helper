package com.fifed.translator.mvp.managers_ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.fifed.architecture_app.constants.BaseFragmentIdentifier;
import com.fifed.architecture_app.mvp.managers_ui.BaseAuthorizotionActivityManagerUI;
import com.fifed.architecture_app.mvp.view_data_pack.core.DataPack;
import com.fifed.architecture_app.mvp.view_notification.ViewNotification;

/**
 * Created by Fedir on 01.12.2016.
 */

public class AuthActivityPortraitManagerUI extends BaseAuthorizotionActivityManagerUI {
    public AuthActivityPortraitManagerUI(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    protected void initUI() {
    }

    @Override
    protected int getIdFragmentsContainer() {
        return 0;
    }

    @Override
    protected int getToolbarContainerID() {
        return 0;
    }

    @Override
    public void changeFragmentTo(BaseFragmentIdentifier fragmentsID, DataPack pack) {

    }

    @Override
    public void initToolbar() {

    }

    @Override
    public int getActivityRootLayout() {
        return 0;
    }

    @Override
    public void onReceiveNotification(ViewNotification notification) {

    }


    @Override
    protected int getToolbarNavigationIcon() {
        return 0;
    }

    @Override
    public void authorizationResult(int requestCode, int resultCode, Intent data) {

    }
}
