package com.fifed.translator.mvp.managers_ui;

import android.support.v7.app.AppCompatActivity;

import com.fifed.translator.R;

/**
 * Created by Fedir on 02.12.2016.
 */

public class ContantActivityLandscapeManagerUI extends ContantActivityPortraitManagerUI {
    public ContantActivityLandscapeManagerUI(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    public int getActivityRootLayout() {
       return R.layout.contant_activity_portret;
    }
}
