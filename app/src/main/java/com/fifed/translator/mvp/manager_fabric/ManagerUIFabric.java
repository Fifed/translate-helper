package com.fifed.translator.mvp.manager_fabric;

import android.support.v7.app.AppCompatActivity;

import com.fifed.architecture_app.mvp.managers_ui.interfaces.core.ManagerUI;
import com.fifed.architecture_app.mvp.managerui_fabrics.ManagerActivityFabric;
import com.fifed.translator.mvp.managers_ui.AuthActivityLandscapeManagerUI;
import com.fifed.translator.mvp.managers_ui.AuthActivityPortraitManagerUI;
import com.fifed.translator.mvp.managers_ui.ContantActivityLandscapeManagerUI;
import com.fifed.translator.mvp.managers_ui.ContantActivityPortraitManagerUI;

/**
 * Created by Fedir on 01.12.2016.
 */

public class ManagerUIFabric extends ManagerActivityFabric {
    @Override
    public ManagerUI getAuthManagerUI(AppCompatActivity activity) {
        if(isPortraitOrientation(activity)){
            return new AuthActivityPortraitManagerUI(activity);
        } else return new AuthActivityLandscapeManagerUI(activity);
    }

    @Override
    public ManagerUI getContentManagerUI(AppCompatActivity activity) {
        if(isPortraitOrientation(activity)){
            return new ContantActivityPortraitManagerUI(activity);
        } else return new ContantActivityLandscapeManagerUI(activity);
    }
}
