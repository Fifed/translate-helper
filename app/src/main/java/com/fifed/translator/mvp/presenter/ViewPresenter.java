package com.fifed.translator.mvp.presenter;

import com.fifed.architecture_app.mvp.presenters.BaseViewPresenter;
import com.fifed.architecture_app.mvp.views.ActivityView;
import com.fifed.architecture_datacontroller.interactor.core.interfaces.InteractorActionInterface;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;
import com.fifed.translator.application.App;

/**
 * Created by Fedir on 01.12.2016.
 */

public class ViewPresenter extends BaseViewPresenter {
    public ViewPresenter(ActivityView activityView) {
        super(activityView);
    }

    @Override
    protected InteractorActionInterface getActionIntarface() {
        return App.getActionInterface();
    }

    @Override
    protected ObservableInteractor getObservable() {
        return App.getObservable();
    }
}
