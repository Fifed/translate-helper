package com.fifed.translator.mvp.managers_ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import com.fifed.architecture_app.constants.BaseFragmentIdentifier;
import com.fifed.architecture_app.mvp.managers_ui.BaseContentActyvityManagerUI;
import com.fifed.architecture_app.mvp.view_data_pack.core.DataPack;
import com.fifed.architecture_app.mvp.view_notification.ViewNotification;
import com.fifed.translator.R;
import com.fifed.translator.constants.FragmentIdentifier;
import com.fifed.translator.fragments.DashBoardFragment;
import com.fifed.translator.fragments.DictionaryFragment;
import com.fifed.translator.fragments.InternetTranslateFragment;

import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public class ContantActivityPortraitManagerUI extends BaseContentActyvityManagerUI {
    public ContantActivityPortraitManagerUI(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    protected void initUI() {
        List<Fragment> fragmentList = getCurrentAddedFragments();
        if(fragmentList.size() == 0){
            getActivity().getSupportFragmentManager().beginTransaction().add(getIdFragmentsContainer(),
                    new DashBoardFragment(), getDashBoardFragmentClass().getSimpleName()).commit();
        }
    }

    @Override
    protected int getIdFragmentsContainer() {
        return R.id.fragment_container;
    }

    @Override
    protected int getToolbarContainerID() {
        return 0;
    }

    @Override
    public void changeFragmentTo(BaseFragmentIdentifier fragmentsID, DataPack pack) {
        switch (((FragmentIdentifier)fragmentsID).getFragmentID()){
            case DASH_BOARD_FRAGMENT:

                break;
            case DICTIONARY_FRAGMENT:
                addFragmentToContainer(new DictionaryFragment(), true, null);
                break;
            case INTERNET_TRANSLATOR_FRAGMENT:
                addFragmentToContainer(new InternetTranslateFragment(), true, null);
                break;
        }
    }

    @Override
    public void initToolbar() {

    }

    @Override
    protected Class<?> getDashBoardFragmentClass() {
        return DashBoardFragment.class;
    }

    @Override
    public int getActivityRootLayout() {
        return R.layout.contant_activity_portret;
    }

    @Override
    public void onReceiveNotification(ViewNotification notification) {

    }

    @Override
    protected int getToolbarNavigationIcon() {
        return 0;
    }
}
