package com.fifed.translator.constants;

import com.fifed.architecture_app.constants.BaseFragmentIdentifier;

/**
 * Created by Fedir on 02.12.2016.
 */

public class FragmentIdentifier implements BaseFragmentIdentifier {
    FragmentID fragmentID;

    public FragmentIdentifier(FragmentID fragmentID) {
        this.fragmentID = fragmentID;
    }

    @Override
    public FragmentID getFragmentID() {
        return fragmentID;
    }
    public enum FragmentID{
        DASH_BOARD_FRAGMENT,
        DICTIONARY_FRAGMENT,
        INTERNET_TRANSLATOR_FRAGMENT
    }
}
