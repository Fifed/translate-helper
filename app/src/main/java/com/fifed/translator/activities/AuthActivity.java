package com.fifed.translator.activities;

import com.fifed.architecture_app.activities.BaseAuthorizationActivity;
import com.fifed.architecture_app.mvp.managers_ui.interfaces.core.ManagerUI;
import com.fifed.architecture_app.mvp.presenters.BaseViewPresenter;
import com.fifed.translator.R;
import com.fifed.translator.mvp.manager_fabric.ManagerUIFabric;
import com.fifed.translator.mvp.presenter.ViewPresenter;

public class AuthActivity extends BaseAuthorizationActivity {

    @Override
    protected ManagerUI getManagerUIToInit() {
        return new ManagerUIFabric().getAuthManagerUI(this);
    }

    @Override
    public BaseViewPresenter getViewPresenter() {
        return new ViewPresenter(this);
    }

    @Override
    public int getExitDoubleClickText() {
        return R.string.for_exit_click_again;
    }
}
