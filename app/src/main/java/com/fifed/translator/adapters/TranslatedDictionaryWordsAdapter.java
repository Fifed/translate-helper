package com.fifed.translator.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fifed.translator.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public class TranslatedDictionaryWordsAdapter extends RecyclerView.Adapter<TranslatedDictionaryWordsAdapter.ViewHolder> {
    private List<String> wordsList;

    public TranslatedDictionaryWordsAdapter(List<String> wordsList) {
        this.wordsList = wordsList;
    }

    public TranslatedDictionaryWordsAdapter setWordsList(List<String> wordsList) {
        this.wordsList = wordsList;
        return this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.translated_dictionary_list_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TranslatedDictionaryWordsAdapter.ViewHolder vh = holder;
        vh.tvTranslatedWords.setText(wordsList.get(position).replace("\n",""));
    }
    public void clearData(){
        wordsList = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return wordsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTranslatedWords;
        public ViewHolder(View v) {
            super(v);
            tvTranslatedWords = (TextView)v.findViewById(R.id.tv_translated_words);
        }
    }
}
