package com.fifed.translator.application;

import com.fifed.architecture_app.application.core.BaseApp;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;
import com.fifed.datacontroller.interactors.TranslateInteractor;

/**
 * Created by Fedir on 01.12.2016.
 */

public class App extends BaseApp {
    @Override
    public ObservableInteractor getInteractor() {
        return TranslateInteractor.getInteractorInstance(getContext());
    }
}
