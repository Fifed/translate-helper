package com.fifed.translator.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.fifed.architecture_app.fragments.core.BaseFragment;
import com.fifed.architecture_app.mvp.view_data_pack.core.DataPack;
import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromDictionaryAction;
import com.fifed.datacontroller.interactions.models.TranslateFromDictionaryModel;
import com.fifed.datacontroller.interactions.models.models_id.TranslateFromDictionaryModelID;
import com.fifed.translator.R;
import com.fifed.translator.adapters.TranslatedDictionaryWordsAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public class DictionaryFragment extends BaseFragment implements TranslateFromDictionaryModelID,TextWatcher {
    private EditText etWordToTranslate;
    private RecyclerView rvTranslatedWords;
    private List<String> itemList;
    private TranslatedDictionaryWordsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int rvPos;
    @Override
    protected int getLayoutResource() {
        return R.layout.dictionary_fragment;
    }

    @Override
    protected View initUI(View v) {
        etWordToTranslate = (EditText)v.findViewById(R.id.et_words_to_translate);
        rvTranslatedWords = (RecyclerView)v.findViewById(R.id.rv_translated_words);
        return v;
    }
    private void initRecyclerView(){
        adapter = new TranslatedDictionaryWordsAdapter(itemList == null ? new ArrayList<String>() : itemList);
        rvTranslatedWords.setLayoutManager(layoutManager = new LinearLayoutManager(getActivity()));
        rvTranslatedWords.setAdapter(adapter);
    }

    @Override
    protected void onFragmentRegisteredAsObserver() {
        initRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        int i = rvTranslatedWords.getChildCount();
        int b = adapter.getItemCount();
        rvTranslatedWords.scrollToPosition(rvPos);
    }

    @Override
    protected void onFragmentUnregisteredAsObserver() {
        int i = rvTranslatedWords.getChildCount();
        rvPos = layoutManager.findFirstCompletelyVisibleItemPosition();
    }

    @Override
    public BaseFragment setDataPack(DataPack pack) {
        return null;
    }

    @Override
    protected void setListeners() {
        etWordToTranslate.addTextChangedListener(this);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    private void handleTranslateFromDictionaryModel(TranslateFromDictionaryModel model){
        adapter.setWordsList(itemList = model.getTranslatedWords().getTranslatedWords());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onUpdateData(Model model) {
        if(model instanceof TranslateFromDictionaryModel){
            handleTranslateFromDictionaryModel((TranslateFromDictionaryModel)model);
        }
    }

    @Override
    public void onError(Error error) {
        boolean test = true;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(s.length() > 1){
            getActionInterface().userMadeAction(new GetTranslateFromDictionaryAction(getCustomTAG(),s.toString()));
        } else if(s.length() == 0){
            adapter.clearData();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
