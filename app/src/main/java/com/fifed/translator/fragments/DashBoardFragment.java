package com.fifed.translator.fragments;

import android.view.View;
import android.widget.Button;

import com.fifed.architecture_app.fragments.core.BaseFragment;
import com.fifed.architecture_app.mvp.view_data_pack.core.DataPack;
import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.translator.R;
import com.fifed.translator.constants.FragmentIdentifier;

/**
 * Created by Fedir on 02.12.2016.
 */

public class DashBoardFragment  extends BaseFragment {
    private Button btnDictionary, btnTranslator;
    @Override
    protected int getLayoutResource() {
        return R.layout.dash_board_fragment;
    }

    @Override
    protected View initUI(View v) {
        btnDictionary = (Button)v.findViewById(R.id.btn_open_dictionary);
        btnTranslator = (Button)v.findViewById(R.id.btn_open_translator);
        return v;
    }

    @Override
    protected void setListeners() {
        btnDictionary.setOnClickListener(this);
        btnTranslator.setOnClickListener(this);
    }

    @Override
    public BaseFragment setDataPack(DataPack pack) {
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_open_dictionary:
                getFragmentFeedBackInterface().changeFragmentTo(new FragmentIdentifier(FragmentIdentifier.FragmentID.DICTIONARY_FRAGMENT), null);
                break;
            case R.id.btn_open_translator:
                getFragmentFeedBackInterface().changeFragmentTo(new FragmentIdentifier(FragmentIdentifier.FragmentID.INTERNET_TRANSLATOR_FRAGMENT), null);
                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onUpdateData(Model model) {

    }

    @Override
    public void onError(Error error) {

    }
}
