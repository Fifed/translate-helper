package com.fifed.translator.fragments;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fifed.architecture_app.fragments.core.BaseFragment;
import com.fifed.architecture_app.mvp.view_data_pack.core.DataPack;
import com.fifed.architecture_datacontroller.interaction.core.Error;
import com.fifed.architecture_datacontroller.interaction.core.Model;
import com.fifed.datacontroller.interactions.actions.GetTranslateFromInternetAction;
import com.fifed.datacontroller.interactions.models.TranslateFromInternetModel;
import com.fifed.datacontroller.interactions.models.models_id.TranslateFromInternetModelID;
import com.fifed.translator.R;

/**
 * Created by Fedir on 02.12.2016.
 */

public class InternetTranslateFragment extends BaseFragment implements TranslateFromInternetModelID, TextWatcher {
    private EditText etTranslateInput;
    private TextView tvTranslatedText;
    @Override
    protected int getLayoutResource() {
        return R.layout.internet_translate_fragment;
    }

    @Override
    protected View initUI(View v) {
        etTranslateInput = (EditText)v.findViewById(R.id.et_int_translate);
        tvTranslatedText = (TextView)v.findViewById(R.id.tv_translated_text);
        return v;
    }

    @Override
    protected void setListeners() {
        etTranslateInput.addTextChangedListener(this);
    }

    @Override
    public BaseFragment setDataPack(DataPack pack) {
        return null;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    private void handleTranslateFromInternetModel(TranslateFromInternetModel model){
        tvTranslatedText.setText(model.getTranslate().getTranslatedText());
    }

    @Override
    public void onUpdateData(Model model) {
        if(model instanceof TranslateFromInternetModel){
            handleTranslateFromInternetModel((TranslateFromInternetModel)model);
        }
    }

    @Override
    public void onError(Error error) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(final CharSequence s, int start, int before, int count) {
        if(s.length() == 0){
            tvTranslatedText.setText("");
        } else {
           tvTranslatedText.postDelayed(new Runnable() {
               @Override
               public void run() {
                   if(getActionInterface() != null){
                       getActionInterface().userMadeAction(new GetTranslateFromInternetAction(getCustomTAG(), s.toString()));
                   }
               }
           }, 1000);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
