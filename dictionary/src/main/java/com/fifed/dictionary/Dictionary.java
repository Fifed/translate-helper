package com.fifed.dictionary;

import android.content.Context;

import com.fifed.dictionary.utils.TextReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public class Dictionary {
    private String wordsSource;
    String wordsSourceLowerCase;
    private static Dictionary dictionary;
    private Dictionary(final Context context)  {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    readTranslateSource(context);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public static void init(Context context)  {
        if(dictionary == null) {
            dictionary = new Dictionary(context);
        }
    }
    public static Dictionary getInstance(){
        if(dictionary == null){
            throw new RuntimeException("First, initialize the static method init!");
        } else return dictionary;
    }
    private void readTranslateSource(final Context context) throws IOException{
            wordsSource = TextReader.readRawText(context, R.raw.translate_source);
            wordsSourceLowerCase = wordsSource.toLowerCase();
    }

    public List<String> searchWords(String word){
        List<String> wordList = new ArrayList<>();
        String wordLowerCase = word.toLowerCase();
        if(wordsSource == null){
            return wordList;
        }

        int indexStart = wordsSourceLowerCase.indexOf("\n"+wordLowerCase);
        int indexEnd = wordsSourceLowerCase.indexOf("\r\n", indexStart);
        if(indexStart == -1){
            return wordList;
        }

        wordList.add(wordsSource.substring(indexStart, indexEnd));
        while (true){
               indexStart = wordsSourceLowerCase.indexOf("\n"+wordLowerCase, indexEnd);
               indexEnd = wordsSourceLowerCase.indexOf("\r\n", indexStart);
            if(indexStart == -1){
                break;
            }
            wordList.add(wordsSource.substring(indexStart, indexEnd));
        }
        Collections.sort(wordList);
        return wordList;
    }
}
