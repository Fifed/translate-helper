package com.fifed.dictionary.utils;

import android.content.Context;
import android.support.annotation.RawRes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Fedir on 01.12.2016.
 */

public class TextReader {
    public static String readRawText(Context context, @RawRes int rawTextFileResId) throws IOException {
        InputStream in = context.getResources().openRawResource(rawTextFileResId);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[4096];
        while ((nRead = in.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
            if(in.available() == 0){
                break;
            }
        }
        buffer.flush();
        return buffer.toString();
    }

}

