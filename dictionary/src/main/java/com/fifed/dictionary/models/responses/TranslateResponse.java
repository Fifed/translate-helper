package com.fifed.dictionary.models.responses;

import com.fifed.dictionary.models.intarfaces.ITranslatedWords;

/**
 * Created by Fedir on 01.12.2016.
 */

public interface TranslateResponse {
    void onSuccsesTranslate(ITranslatedWords iTranslatedWords);
    void onErrorTranslate(Throwable throwable);
}
