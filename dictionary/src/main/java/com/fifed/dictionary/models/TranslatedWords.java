package com.fifed.dictionary.models;

import com.fifed.dictionary.models.intarfaces.ITranslatedWords;

import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public class TranslatedWords implements ITranslatedWords{
    private List<String> translatedWordList;

    public TranslatedWords(List<String> translatedWordList) {
        this.translatedWordList = translatedWordList;
    }

    @Override
    public List<String> getTranslatedWords() {
        return translatedWordList;
    }
}
