package com.fifed.dictionary.models.intarfaces;

import java.util.List;

/**
 * Created by Fedir on 01.12.2016.
 */

public interface ITranslatedWords {
    List<String> getTranslatedWords();
}
